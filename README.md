# Dockerized Crystal Compiler
Dockerized crystal compiler.

Branches:
- [x] **main** compiles statically against musl-libc
- [x] **main-musl** compiles dynamically against musl-libc
- [ ] **main-glib** compiles dynamically against glibc

## Build & Install
```bash
make && sudo make install
```

## License
Copyright 2021 Julian Kahlert

Use of this source code is governed by the MIT
license that can be found in the LICENSE file or at
https://opensource.org/licenses/MIT.
